# S M Habib Ullah
habib43005@gmail.com
Bronx, New York 10458

## SUMMERY
Hands on working experience on Deploying Micro-Service based application which contains one PostgreSQL Database server along with 6 others different services on both Bare-Metal and Container platform.
• Completed projects on PHP-Multilingual-Blog-Application on Self-managed and AWS Managed RDS which includes private subnet for backend DB with NAT Gateway, public subnet for PHP frontend application.
• Experienced in working with AWS Cloud administer in various services like EC2, VPC, S3, EBS, ELB, AMI, Route 53, Auto scaling, load balancer, RDS, DynamoDB, IAM, Cloud Watch, and Security Groups.
• Hands on experience with container deployments by using Kubernetes, Docker, Dockerfile, Docker Compose, Docker Hub and Docker images. 
• Deployed Two-tier-Application which has Nodejs as Frontend and Python as Backend service on Bare-Metal, Docker and Kubernetes Containers using multiple PODs. 
• Hands on experience on KUBCTL, AWS CLI, EKSCTL for accessing and configuring K8S cluster from terminal. 
• Sufficient knowledge on deploying and managing Kubernetes monitoring tools like Prometheus and Grafana. 
• Experienced with version control tools like GIT, Bitbucket and for branching and maintaining the version across the environments. 
• Extensive knowledge on Continuous Integration and Continuous Delivery (CI/CD) in the Software Development Lifecycle using GitLab. • Deployed a complete automated CI/CD pipeline using Git Hub as the code repo, Terraform for creating Infrastructure on GitLab. • Completed basic projects on Terraform as an automation tool. • Configured Inbound/Outbound in AWS Security groups according to the requirements. • Experienced in maintaining and configuring of IAM roles on AWS. • Experience in configuring the monitoring and alerting tools according to the requirement like AWS CloudWatch. • Hands-on experience in deploying developed code in Nginx, Apache. • excellent communication analytical and problem-solving skills, with the ability to work within a team environment and independently. • Managed the Azure AD infrastructure and collaborate with various teams to ensure it integrates seamlessly with all components including network, security, and remote access. • Intermediate knowledge on Azure Cloud Services including Azure AD, VM, Resource group, Blob Storage. • Load balancer configuration, setup and monitoring in Azure • Extensive knowledge on ESX-i Host, Hypervisor management and administration. • Experience on Veeam Server Backup Software. • Intensive experience in Windows Server 2016, Multi-Forest, Multi-Domain, Multi-Site Infrastructure, Active Directory infrastructure and maintenance, Active Directory Replication, DNS Resolution, DNS Backup and Restore, DHCP Server Implementation and management, FSMO Role, Failover-Cluster, WSUS. • Creating, Assigning and Maintaining GPO along with ADMX, ADML file integration. • File Server Creating, Maintaining and Troubleshooting.
• Full System Backup and restore including GPO, AD. • Basic knowledge on Power Shell. • Print server setup, configure and monitoring. • Experience with O365 Admin Console and Microsoft Endpoint Manager, share point. • Advanced understanding of Content Distribution and Management, Maintenance Windows, Queries, Application Deployment, Packaging Deployment, Operating Systems Deployment, Microsoft Software Updates, and 3rd - Party Integrations.
• Responsible for designing, administering, building, testing, and upgrading initiatives for the Active Directory infrastructure platform
• Extensive experience with Windows Server 2016 for administering Active Directory in an enterprise class environment.
• Special proficiency in System installation, configuration, implementation, maintenance, security setup, and troubleshooting
• Coordinate system and server installations and upgrades to ensure work is performed in accordance with company policy • STRONG AD and Application troubleshooting abilities. • Ability to fully functional Operating System Deployment, Application Installs and configuration changes. • Experience with Active Directory User and Computer accounts, Security Groups, and group policy objects.

## TECHNICAL SKILLS
Docker, Kubernetes, Docker swarm, Docker –Hub.
• AWS Administration, VPC, Route 53, S3
• Terraform, GitLab CI
• Azure Administration, O365 Administration
• ESX-i Host, Hypervisor
• Windows Server 2016, Multi-Forest, Multi-Domain, Multi-Site Infrastructure,
• AD Infrastructure, AD Replication
• DNS Resolution, DNS Backup and Restore.
• DHCP Server, FSMO Role,
• Failover-Cluster, WSUS, AD
• GPO, ADMX, ADML, File Server
• Microsoft Windows 7/8/10/ 2012 R2 2016
• Network TCP/IP, DNS, DHCP, GPOs
• Application support
• Service Now & Footprint ticketing system
• Active Directory and Office 365
• Cisco AnyConnect and Global Protect VPN
• Smart Deploy and Intune (Imaging)
• LogMeIn, Tight VNC

## EXPERIENCE

## Nagarro Inc, NYC – IT Support Administrator                            Dec 2021 – Present
Provide Level 2 technical support, maintenance troubleshooting of desktop, printer and telephones.
• Completed New User setup including Outlook, One Drive, MS Teams, necessary software setup for the Windows and Mac OS environment.
• Used MS Teams in day-to-day team conversation, weekly meeting, reaching out user, remotely access end user’s computer, group creation, video recording and backup.
• Respond hotline call and ticketing system to complete work orders in a timely manner to meet department and customer service objects.
• Set up and troubleshoot wide range computer hardware and software issues Windows, Mac, Linux operating system and Peripherals.
• Responsible for Network connectivity, Network Infrastructure troubleshooting, Biometric Access to the users.
• Responsible for IT Audit for US region and successfully completed ISO Audit 2022.
• Support onsite users for setting up laptops and peripherals and act as a first point of contact for the users in US region.
• Maintain IT inventory for laptops and support for maintaining other assets.
• Actively working with new joiners’ team for providing IT assets and complete setup according to company policy.
• Work as a part of Intune team for making sure all the devices are enrolled properly.

## Pro Unlimited, NYC – IT Support Administrator                              June 2021 – Dec 2021
Manage On premises Active Directory, Azure Active Directory, and Office 365 suite.
• Completed New User setup including Outlook, One Drive, MS Teams, necessary software setup for the Windows and Mac OS environment. • Manage On premises Active Directory, Azure Active Directory, and Office 365 suite.Install Standard Image on Desktops and Laptops through Auto pilot (Intune).
• Added new computer to domain server for successful access to user account from the new computer.
• In-depth knowledge about OS deployment using Smart Deploy on premises and remote environment.
• Excellent knowledge about Active Directory in terms of User creation, assign group permission, Group policy.
• Used Verizon portal for ordering cell phones, assigning plans, suspended or extended plans.
• Assisted setting up Microsoft Authenticator App for Multi-factor Authentication service.
• Experienced in encryption tools like Bit Locker and encryption key backup in backup server like Secret Server.
• Managed O365 Admin Centre. Adding, deleting users, Creating new mailbox in the shared mailbox.
• Adding, removing, allowing permission to users in Shared Mailbox.
• Detailed experience about Azure Active Directory in terms of creating user account, activate or deactivate it, adding users in security groups and so on.
• Purchase, assign license and install Adobe Acrobat Pro DC, Adobe Creative Cloud to the user.
• Purchase, assign license and install MS Project, MS VISIO to the end user to ensure work flow.
• Experienced installing, supporting VPN services like Global Protect, AnyConnect, Citrix workspace.
• Used remote connecting tools such as Log Me In, Remote Desktop Service for connecting the user and servers.
• Assisted Driver Mapping, provided user access to specific driver security group for file sharing.
• Performed Power Shell command to store Hardwire ID for implementing new laptop setup through Intune.
• Supported users by providing step by step guide to complete new iPhone setting according to company policy.

## Barclays, NYC – IT Support Administrator                                  Dec 2019 – May 2021
Diagnose hardware performance and remove negative parts and maintain reporting for remediation.
• Managed end-user accounts and permissions, overseeing correct provisioning of access rights in Active Directory in accordance with security best practices and policies.
• Monitor service now ticketing system and make sure with proper notes to resolve or escalate.
• Coordinate with Networking team and troubleshooting cabling, patching, racking and Layer one issues
• Documented all transactions and support interactions in system for future reference and addition to knowledge.
• Maintain IT inventory ensure items are documented with service tag and ticket no.
• Enroll laptop and ensure devices are properly enrolled with all necessary information
• Configured hardware, devices and software to set up workstations for employees.
• Removed malware, ransomware and other threats from laptops and desktop systems.
• Followed up with clients to verify optimal customer satisfaction following support engagement and problem resolution.
• Assist in desktop imaging and moving projects in different location and ensure data backup properly.

## Northwell Health, NYC– IT Support Administrator                            Sep 2017- Nov 2019
Provide end user support ensured proper maintenance of workstations, printers, scanner, and peripherals.
• Installing operating systems such as Windows 7, Windows 8, Windows 10, Linux, and Mac.
• Diagnose, troubleshoot, and resolve a wide range of software, hardware, and network issues
• Configuring and troubleshooting various iMac Hardware and software issues.
• Create desktop image, ensure data backup and data restore when replacing new PC
• Imaging desktop and laptop using Clonezilla USB drive and network PXE.
• Lead pc moving project and migrate windows 7 to windows 10.
• Creating and updating network shares, adding permissions for software and groups through Active.
• Resetting password, enable, disable, and update information in Active Directory.
• Resetting email password, assigning license, block and unblock email accounts through office 365.
• Install office application and troubleshoot wide range of word, excel and outlook issues.
• Troubleshoot, setup Cisco IP Phone, Avaya and working with service providers for escalation.
• Troubleshoot layer one cabling issues and patching cable for data closet.
• Working with networking team to troubleshoot and cabling switches, firewall and servers in networking racks.
• Setup conference room and troubleshoot AV issues including WebEx, zoom and skypes.
• Created, responded to, escalate, and close tickets with notes and track all outstanding tickets with System Admin to ensure problems are resolved in timely manner.
• Performed face-to-face and telephone support for software and hardware issues.
• Experience in installing various anti-virus software and handling computer security issues

## Al Arafah Bank Ltd, BD– Desktop Administrator                             Sep 2015- August 2017
Build, configure and deploy Windows 7, 10 Desktops and laptops. 
• Setup complete connectivity of to office network for remote location. 
• Computer lab management and managing workgroup environment. 
• Troubleshoot wide range phone, computer and printer hardware and software issues. 
• Monitor and quickly respond helpdesk request and incident through hotline call and ticketing system. 
• Serve as the first point of contact for customer seeking technical assistance. 
• Walk the customer through the problem-solving process and escalate the next level with proper notes 
• Reset password, unlock, and update user information in Active Directory

## EDUCATION

## BRAC University, Bangladesh
Bachelor’s in Electronic and Communication Engineering
## AZ-104 (Microsoft Certified Azure Administrator Associate)
ID: 1287-9683
## CCNA (Routing & Switching)
Cisco Certified Network Associate
ID: CSCO13662983